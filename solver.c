/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: popanase <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 21:20:37 by popanase          #+#    #+#             */
/*   Updated: 2018/03/01 21:20:44 by popanase         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_count_way(t_cord *veh, t_ride *ride);
{
	int	w;

	w = ABS(ABS(veh->cur->x - ride->start->x) - (ride->finish->x));
	w += ABS(ABS(veh->cur->y - ride->start->y) - (ride->finish->y));
	return(w);
}

void	ft_select_ride(t_veh *veh)
{
	int	i;
	int	min;

	i = 0;
	min = veh->rides[i];
	while (veh->rides[i])
	{
		if (veh->rides[i] < min)
			min = veh->rides[i];
		i++;
	}
	veh->ride = min;
}

void		ft_count_veh(t_map *map)
{
	int	i;
	int	j;
	int k;

	i = 0;
	j = 0;
	k = 0;
	while(map->veh[i])
	{
		while(map->rides[j])
		{
			while(map->veh[i]->rides[k])
			{
				map->veh[i]->rides[k] = ft_count_way(map->veh[i]->cur, map->rides[j]);
				k++;
			}
			j++;
		}
		ft_select_ride(map->veh[i]);
		map->rides[map->veh[i]->ride_ind]->er_st += map->veh[i]->rides[map->veh[i]->ride_ind];
		i++;
	}
}

void	ft_handle_veh(t_map *map)
{
	int	i;

	i = 0;
	while(map->veh[i])
	{
		if (!map->rides[map->veh[i]->ride_ind]->er_st)
		{

		}
		else 
			map->rides[map->veh[i]->ride_ind]->er_st--;
		i++;
	}
}

void	ft_solver(t_map *map)
{
	int	i;

	i = 0;
	ft_count_veh(t_map *map);
	while (i < map->steps)
	{
		ft_handle_veh(t_map *map);
		ft_count_veh(t_map *map);
		i++;
	}
}