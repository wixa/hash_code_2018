/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c	 	                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 10:16:49 by kshyshki          #+#    #+#             */
/*   Updated: 2018/02/22 10:16:54 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	main(void)
{
	t_map	*map;
	char 	*ln;
	char 	**spl_line;
	t_ride	*ride;
	int 	i;

	i = 0;
	if (!(map = (t_map*)malloc(sizeof(t_map))) || !get_next_line(0, &ln))
		return (1);
	ft_bzero(map, sizeof(t_map));
	if (ft_len_arr_str((const char**)(spl_line = ft_strsplit(ln, ' '))) != 6)
		return (1);
	map->row = ft_atoi(spl_line[0]);
	map->col = ft_atoi(spl_line[1]);
	map->veh = ft_atoi(spl_line[2]);
	map->rides_nbr = ft_atoi(spl_line[3]);
	map->bon = ft_atoi(spl_line[4]);
	map->steps = ft_atoi(spl_line[5]);
	if (!(map->rides = (t_ride**)malloc(sizeof(t_ride) * (map->rides_nbr + 1))))
		return (1);
	map->rides[map->rides_nbr] = NULL;
	ft_bzero(map->rides, sizeof(t_rd_lst));
	while (get_next_line(0, &ln))
	{
		if (ft_len_arr_str((const char**)(spl_line = ft_strsplit(ln, ' '))) != 6)
			return (1);
		ride = (t_ride*)malloc(sizeof(t_ride));
		ride->start = (t_coord*)malloc(sizeof(struct s_cord));
		ride->finish = (t_coord*)malloc(sizeof(struct s_cord));
		ride->start->x = ft_atoi(spl_line[0]);
		ride->start->y = ft_atoi(spl_line[1]);
		ride->finish->x = ft_atoi(spl_line[2]);
		ride->finish->y = ft_atoi(spl_line[3]);
		ride->er_st = ft_atoi(spl_line[4]);
		ride->lt_fin = ft_atoi(spl_line[5]);
		map->rides[i++] = ride;
	}
	i = 0;
	map->t_veh = (t_veh**)malloc(sizeof(t_veh*) * (map->veh + 1));
	map->t_veh[map->veh] = NULL;
	while (i < map->veh)
	{
		map->t_veh[i] = (t_veh*)malloc(sizeof(t_veh));
		map->t_veh[i]->cur = (t_coord*)malloc(sizeof(struct s_cord));
		map->t_veh[i]->cur->x = 0;
		map->t_veh[i]->cur->y = 0;
		map->t_veh[i]->rides = (int*)malloc(sizeof(int) * map->rides_nbr + 1);
		map->t_veh[i]->rides[map->rides_nbr] = NULL;
		ft_bzero(map->t_veh[i]->rides, sizeof(int));
		map->t_veh[i++]->ride_ind = -1;
	}
	//tut start
	return (0);
}