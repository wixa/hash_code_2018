/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pizza.h 	                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 10:17:14 by kshyshki          #+#    #+#             */
/*   Updated: 2018/03/01 16:39:51 by popanase         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "libft/ft_printf.h"
# include "libft/get_next_line.h"
# define ABS(x) (x > 0) ? x : -x

typedef struct			s_rd_lst
{
	struct	s_ride		*ride;
	struct	s_rd_lst	*next;
}						t_rd_lst;

typedef struct			s_cord
{
	int					x;
	int 				y;
}						t_coord;

typedef struct			s_ride
{
	struct s_cord		*start;
	struct s_cord		*finish;
	int 				er_st;
	int 				lt_fin;
}						t_ride;

typedef	struct 			s_veh
{
	int					*rides;
	struct s_cord		*cur;
	int					ride_ind;
}						t_veh;

typedef struct 			s_map
{
		int				row;
		int 			col;
		int 			veh;
		int 			rides_nbr;
		int 			bon;
		int 			steps;
		t_ride			**rides;
		t_veh  			**t_veh;
}						t_map;

typedef	struct  		s_res_out
{
	char	*res;
	t_res 	*next;
}						t_res_out;

<<<<<<< HEAD
int		ft_count_way(t_cord *veh, t_ride *ride);
int		ft_count_veh(t_map *map);
void	ft_select_ride(t_veh *veh);
=======
int		ft_count_way(t_coord *veh, t_ride *ride);
int		ft_count_veh(t_veh *veh, t_map *map);
>>>>>>> 22ed1344be930de098f0f5536b3288018a27a971
void	ft_update_veh_lst(t_map *m, t_veh);
void	ft_get_rides(t_map *m, t_ride);

#endif
